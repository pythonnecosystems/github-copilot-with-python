# GitHub Copilot: 생각의 속도로 Python으로 비행하기 <sup>[1](#footnote_1)</sup>

## 목차
- [Python에서 GitHub Copilot 시작하기](./github-copilot-with-python.md#python에서-github-copilot-시작하기)
    - [GitHub Copilot 구독](./github-copilot-with-python.md#github-copilot-구독)
    - [Visual Studio Code 확장 프로그램 설치](./github-copilot-with-python.md#visual-studio-code-확장-프로그램-설치)
    <!-- [PyCharm 플러그인 설치]() -->
    - [GitHub Copilot에 고삐 넘기기](./github-copilot-with-python.md#github-copilot에-고삐-넘기기)
- [자연어에서 Python 코드 합성하기](./github-copilot-with-python.md#자연어로부터-python-코드-합성하기)
    - [Python 주석을 사용하여 문제 설명하기](./github-copilot-with-python.md#python-주석을-사용하여-문제-설명하기)
    - [더 많은 주석을 추가하여 문제 복잡도 높이기](./github-copilot-with-python.md#더-많은-주석을-추가하여-문제-복잡도-높이기)
    - [프로그래밍 경시대회 퍼즐 풀기](./github-copilot-with-python.md#프로그래밍-경시대회-퍼즐-풀기)
- [더욱 지능적인 코드 완성 제안 받기](./github-copilot-with-python.md#더욱-지능적인-코드-완성-제안-받기)
    - [GitHub Copilot이 내 마음을 읽도록 하기](./github-copilot-with-python.md#github-copilot이-내-마음을-읽도록-하기)
    - [더 나은 제안을 위한 컨텍스트 제공](./github-copilot-with-python.md#더-나은-제안을-위한-컨텍스트-제공)
    - [GitHub Copilot의 창의력 활용하기](./github-copilot-with-python.md#github-copilot의-창의력-활용하기)
    - [개인 번역기로 여러 프로그래밍 언어를 구사 하기](./github-copilot-with-python.md#개인-번역기로-여러-프로그래밍-언어를-구사-하기)
- [가상 버디와 함께 페어 프로그래밍 연습하기](./github-copilot-with-python.md#가상-버디와-함께-페어-프로그래밍-연습하기)
    - [테스트용 샘플 데이터 fixture 생성하기](./github-copilot-with-python.md#테스트용-샘플-데이터-fixture-생성하기)
    - [테스트 케이스가 마법처럼 나타나길 바라기](./github-copilot-with-python.md#테스트-케이스가-마법처럼-나타나길-바라기)
    - [테스트 중심 개발(TDD) 연습하기](./github-copilot-with-python.md#테스트-중심-개발tdd-연습하기)
- [즉각적인 컨텍스트 인식 솔루션을 위한 Stack Overflow 제거하기](./github-copilot-with-python.md#즉각적인-컨텍스트-인식-솔루션을-위한-stack-overflow-제거하기)
    - [상용구 코드에 대해 다시는 생각하지 않기](./github-copilot-with-python.md#상용구-코드에-대해-다시는-생각하지-않기)
    - [항상 손끝에서 API 문서 확인 가능](./github-copilot-with-python.md#항상-손끝에서-api-문서-확인-가능)
    - [GitHub Copilot에게 고유한 방언을 사용하도록 가르치기](./github-copilot-with-python.md#github-copilot에게-고유한-방언을-사용하도록-가르치기)
    - [익숙하지 않은 프레임워크 또는 라이브러리 탐색하기](./github-copilot-with-python.md#익숙하지-않은-프레임워크-또는-라이브러리-탐색하기)
- [GitHub Copilot 사용에 반대하는 주장 고려하기]()
    - [오토파일럿은 절대 아니다!]()
    - [잠재적인 보안 위험이 있다]()
    - [지적 재산권 문제 제기]()
    - [부정행위 조장]()
    - [구독 플랜이 필요]()
- [마치며]()

**GitHub Copilot**은 코드 편집기에 **인공지능**으로 구동되는 가상 조수를 제공한다는 흥미로운 신기술로, 일반 대중에게 공개되었을 때 상당한 논란을 불러 일으켰다. Python은 이 도구가 특히 잘 지원되는 언어 중 하나이다. 이 튜토리얼을 읽고 나면 GitHub Copilot이 위험인지, 속임수인지, 아니면 소프트웨어 엔지니어링의 진정한 게임 체인저인지 알 수 있기를 기대한다.

**이 튜토리얼에서는 어떻게를 배울 것이다**.

- 코드 편집기에 **GitHub Copilot 확장 프로그램** 설치하기
- 작업에 대한 **자연어** 설명을 작업 코드로 변환하기
- 여러 가지 **지능형 완성된 코드** 대체 제안 중에서 선택하기
- **익숙하지 않은 프레임워크**와 프로그래밍 언어 탐색하기
- GitHub Copilot에게 **커스텀 API** 사용 방법 가르치기
- **가상 페어 프로그래머**와 함께 실시간으로 테스트 중심 개발 연습하기

이 튜토리얼을 계속 진행하려면 개인 [GitHub](https://github.com/) 계정과 [Visual Studio Code](https://realpython.com/python-development-visual-studio-code/)와 같은 코드 편집기 또는 [PyCharm](https://realpython.com/pycharm-guide/)과 같은 통합 개발 환경이 필요하다.

> **무료 다운로드**: 여기를 클릭하여 GitHub Copilot으로 더욱 빠르게 코딩할 수 있는 **[키보드 단축키들에 대한 치트 시트를 무료로 다운로드](https://realpython.com/bonus/github-copilot-python-pdf/)할 수 있다**.

<a name="footnote_1">1</a>: [GitHub Copilot: Fly With Python at the Speed of Thought](https://realpython.com/github-copilot-python/)을 편역한 것이다.
